# ![Eye Bleacher Bot](https://github.com/cjgetty/eyebleacher/blob/master/banner.png)
A bot for Reddit that provides eye bleach when necessary. **Eye Bleacher Bot** monitors various subreddits such as [/r/watchpeopledie](http://reddit.com/r/watchpeopledie), [/r/morbidreality](http://reddit.com/r/morbidreality), and other grim subreddits. Had enough internet for the day? Use Eye Bleacher Bot!

## Usage

### Subreddits
Currently, Eye Bleacher Bot is actively monitoring eight (8) subreddits:
1. [/r/WatchPeopleDie](http://reddit.com/r/watchpeopledie)
2. [/r/MorbidReality](http://reddit.com/r/morbidreality)
3. [/r/WTF](http://reddit.com/r/wtf)
4. [/r/FearMe](http://reddit.com/r/FearMe)
5. [/r/HorriblyDepressing](http://reddit.com/r/horriblydepressing)
6. [/r/Creepy](http://reddit.com/r/creepy)
7. [/r/TrueCreepy](http://reddit.com/r/truecreepy)
8. [/r/ScaredShitless](http://reddit.com/r/scaredshitless)


If you have suggestions for subreddits to monitor, [please create an issue thread](https://github.com/cjgetty/eyebleacher/issues).

### Keywords
Eye Bleacher Bot will respond to any of the following keywords if mentioned in the subreddits listed above:
* *eyebleach*
* *eye bleach*
* *enough internet for the day*
* *enough internet for today*

If you have any suggestions for keywords and/or strings to search for, [please create an issue thread](https://github.com/cjgetty/eyebleacher/issues).

### Warning
*Please refrain from using your own version of Eye Bleacher Bot, as we don't want subreddits being spammed.*

## Built With
1. [Python 2.7.15](https://www.python.org/)
2. [PRAW](https://praw.readthedocs.io/en/latest/)
3. [Dog API](https://dog.ceo/dog-api/)
4. [Atom](https://atom.io/)

## License
Eye Bleacher Bot is currently licensed under The MIT License. For more information, refer to the [LICENSE.md](https://github.com/cjgetty/eyebleacher/blob/master/LICENSE.md).
