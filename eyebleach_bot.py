import praw
import config
import time
import os
import requests

def bot_login():
    print "Logging in..."
    r = praw.Reddit(username = config.username,
            password = config.password,
            client_id = config.client_id,
            client_secret = config.client_secret,
            user_agent = "/u/'s Eye Bleacher Bot v0.1")
    print "Logged in!"

    return r

def run_bot(r, comments_replied_to):
    print "Obtaining 25 comments..."

    keywords = {"eyebleach", "eye bleach", "enough internet for today", "enough internet for the day"}
    for comment in r.subreddit('watchpeopledie+morbidreality+wtf+fearme+horriblydepressing+truecreepy+creepy+scaredshitless').comments(limit=25):
        for keyword in keywords:
            if keyword in comment.body and comment.id not in comments_replied_to and comment.author != r.user.me():
                print "Keyword found in comment " + comment.id + "!"

                eyebleach = requests.get('https://dog.ceo/api/breeds/image/random').json()['message']

                comment_reply = "[Need some eye bleach?](%s)" % eyebleach

                comment_reply += "\n\n^(This eye bleach is humbly provided by) [^(Dog API)](https://dog.ceo/)^(. Eye Bleacher Bot created by) [^(Cameron Getty)](http://github.com/cjgetty)^(.)"

                comment.reply(comment_reply)
                print "Replied to comment " + comment.id + "!"

                comments_replied_to.append(comment.id)

                with open ("comments_replied_to.txt", "a") as f:
                    f.write(comment.id + "\n")

        print "Sleeping for 10 seconds..."
        #Sleep for 10 seconds...
        time.sleep(10)

def  get_saved_comments():
    if not os.path.isfile("comments_replied_to.txt"):
        comments_replied_to = []
    else:
        with open("comments_replied_to.txt", "r") as f:
            comments_replied_to = f.read()
            comments_replied_to = comments_replied_to.split("\n")
            comments_replied_to = filter(None, comments_replied_to)

    return comments_replied_to

r = bot_login()
comments_replied_to = get_saved_comments()
print comments_replied_to

while True:
    run_bot(r, comments_replied_to)
